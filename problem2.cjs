// Problem 2:

//     Using callbacks and the fs module's asynchronous functions, do the following:
//         1. Read the given file lipsum.txt
//         2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
//         3. Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt
//         4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt
//         5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.


const fs = require('fs');

function fsProblem2(filePath) {
    fs.readFile(filePath, "utf-8", (err, data) => {
        if (err) {
            console.log("error in 1")
        }
        else {
            console.log("Your File is ready to read")

            // convert the content to uppercase
            let upperCaseContent = data.toUpperCase();
            fs.writeFile("upperCase.txt", upperCaseContent, (err) => {
                if (err) {
                    console.log("error in 2")
                }
                else {
                    fs.writeFile("filenames.txt", "upperCase.txt", (err) => {
                        if (err) {
                            console.log("error in 3")
                        }
                        else {
                            //console.log("sucess")

                            // read a file and convert it into a lowercase
                            fs.readFile("upperCase.txt", 'utf-8', (err, data) => {
                                if (err) {
                                    console.log("error 4")
                                }
                                else {
                                    // console.log(typeof data);

                                    let lowerConvert = data.toLowerCase().split('.');
                                    let lowerString = lowerConvert.map(lower => lower.trim()).join("\n");
                                    console.log(lowerConvert)
                                    fs.writeFile("lowerCase.txt", lowerString, (err) => {
                                        if (err) {
                                            console.log("error 5")
                                        }
                                        else {
                                            fs.appendFile('./filenames.txt', "\nlowerCase.txt", (err) => {
                                                if (err) {
                                                    console.log("error 6")
                                                }
                                                else {
                                                   // console.log("sucess 3")

                                                    fs.readFile("lowerCase.txt", "utf-8", (err, data) => {
                                                        if (err) {
                                                            console.log("error 7")
                                                        }
                                                        else {
                                                            //console.log("success 4")
                                                            let sortContent = data.split("\n")
                                                            //    console.log(sortContent)

                                                            sortContent = sortContent.sort().join('\n');
                                                            //console.log(sortContent)
                                                            //console.log("is success sort")
                                                            fs.writeFile("sortfile.txt", sortContent, (err) => {
                                                                if (err) {
                                                                    console.log("error 8")
                                                                }
                                                                else {
                                                                    //console.log("sucessfull")
                                                                    fs.appendFile('./filenames.txt', "\nsortfile.txt", (err) => {
                                                                        if (err) {
                                                                            console.log("error 9")
                                                                        }
                                                                        else {
                                                                           // console.log("complete 1")
                                                                            fs.readFile('filenames.txt', 'utf-8', (error, data) => {
                                                                                if (error) {
                                                                                    console.log(error)
                                                                                }
                                                                                let numberOfFiles = data.trim().split('\n');
                                                                                numberOfFiles.forEach((eachFile) => {
                                                                                    fs.unlink(eachFile, (error) => {
                                                                                        if (error) {
                                                                                            console.log(error);
                                                                                        }
                                                                                        else {
                                                                                            console.log(`${eachFile} deleted successfully`);
                                                                                        }

                                                                                    })
                                                                                })
                                                                            })
                                                                        }
                                                                    })


                                                                }
                                                            })
                                                        }
                                                    })
                                                }

                                            })
                                        }
                                    })
                                }
                            })
                        }
                    })
                }
            })
        }
    })
}
module.exports = fsProblem2;
