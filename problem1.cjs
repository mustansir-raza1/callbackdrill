/*
    Problem 1:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Create a directory of random JSON files
        2. Delete those files simultaneously 
*/

const fs = require('fs')
function createJSONFile(dirPath) {

    fs.mkdir(dirPath, { recursive: true }, (err) => {
        if (err) {
            console.log(err);
        }
        else {
            console.log("Directory Created!");
        }


        fs.writeFile("firstFile.txt", 'my first file', (err) => {
            if (err) {
                console.log(err)
            }
            else {
                console.log("first file is created successfully")
                fs.unlink("firstFile.txt", (err) => {
                    if (err) {
                        console.log(err)
                    }
                    else {
                        console.log("your first file is deleted sucessfully")

                        fs.writeFile("secondFile.txt", 'my second file', (err) => {
                            if (err) {
                                console.log(err)
                            }
                            else {
                                console.log("your second file is created sucessfulluy")
                                fs.unlink("secondFile.txt", (err) => {
                                    if (err) {
                                        console.log(err)
                                    }
                                    else {
                                        console.log("your second file is deleted sucessfully")
                                    }
                                })

                            }
                        })
                    }
                })
            }
        });
  
    })
}
// createJSONFile("./Random_JSON_Files");
module.exports = createJSONFile;